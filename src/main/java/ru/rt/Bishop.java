package ru.rt;

import java.math.BigInteger;

public class Bishop extends Chess{

    BigInteger a = new BigInteger("72340172838076673");
    BigInteger h = new BigInteger("9259542123273814144");

    @Override
    BigInteger prepare() {
        BigInteger k = one.shiftLeft(startPos.intValue());
        BigInteger v = new BigInteger("0");
        for (int i = 0; i <= 8; i++) {
            v = v.or(k.shiftLeft(8*i).and(max).or(k.shiftRight(8*i).and(max)));
        }

        BigInteger left = left(v);
        BigInteger right = right(v);

        BigInteger n = new BigInteger("0");
        for (int i = 1; i <= 8; i++) {
            n = n.or(k.shiftLeft(7*i).and(left).and(max).or(k.shiftLeft(9*i).and(right).and(max)))
                    .or(k.shiftRight(7*i).and(right).and(max).or(k.shiftRight(9*i).and(left).and(max)));
        }
        BigInteger m = k.not().and(n);
        return m;
    }

    private BigInteger right(BigInteger v) {
        BigInteger rorig = new BigInteger(v.toString());
        BigInteger right = new BigInteger("0");
        while (true) {
            right = right.or(v);
            if (v.equals(h))
                break;
            v = v.shiftLeft(1);
        }
        right = right.xor(rorig);
        return right;
    }

    private BigInteger left(BigInteger v ){
        BigInteger vorig = new BigInteger(v.toString());
        BigInteger left = new BigInteger("0");
        while (true) {
            left = left.or(v);
            if (v.equals(a))
                break;
            v = v.shiftRight(1);
        }
        left = left.xor(vorig);
        return left;
    }
}