package ru.rt;

import java.math.BigInteger;

abstract class Chess implements Task{
    protected BigInteger max = new BigInteger("18446744073709551615");

    protected BigInteger startPos;
    protected BigInteger one = new BigInteger("1");


    @Override
    public String run(String[] data) {
        startPos = new BigInteger(data[0]);
        BigInteger m = prepare();
        return popcnt(m)+"\r\n"+m+"\r\n";
    }

    private int popcnt(BigInteger bits){
        int cnt = 0;
        while (bits.compareTo(new BigInteger("0")) == 1){
            if(bits.and(one).equals(one)){
                cnt++;
            }
            bits = bits.shiftRight(1);
        }
        return cnt;
    }

    abstract BigInteger prepare();

}
