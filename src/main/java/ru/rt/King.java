package ru.rt;

import java.math.BigInteger;

public class King extends Chess{

    @Override
    BigInteger prepare() {
        BigInteger noa = new BigInteger("18374403900871474942");
        BigInteger noh = new BigInteger("9187201950435737471");
        BigInteger k = one.shiftLeft(startPos.intValue());
        BigInteger ka = noa.and(k);
        BigInteger kh = noh.and(k);
        BigInteger m = k.not().and(
                ka.shiftLeft(7).and(max).or(k.shiftLeft(8).and(max)).or(kh.shiftLeft(9).and(max))
                        .or(ka.shiftRight(1).and(max))               .or(kh.shiftLeft(1).and(max))
                        .or(ka.shiftRight(9).and(max)).or(k.shiftRight(8).and(max)).or(kh.shiftRight(7).and(max))
        );
        return m;
    }


}
