package ru.rt;

import java.math.BigInteger;

public class Knight extends Chess{
    @Override
    BigInteger prepare() {
        BigInteger noa = new BigInteger("18374403900871474942");
        BigInteger noab = new BigInteger("18229723555195321596");
        BigInteger noh = new BigInteger("9187201950435737471");
        BigInteger nohg = new BigInteger("4557430888798830399");
        BigInteger k = one.shiftLeft(startPos.intValue());
        BigInteger ka = noa.and(k);
        BigInteger kab = noab.and(k);
        BigInteger kh = noh.and(k);
        BigInteger khg = nohg.and(k);
        BigInteger m = k.not().and(
                ka.shiftLeft(15).and(max).or(kh.shiftLeft(17).and(max))
                    .or(kab.shiftLeft(6).and(max)).or(khg.shiftLeft(10).and(max))
                    .or(khg.shiftRight(6).and(max)).or(kab.shiftRight(10).and(max))
                    .or(kh.shiftRight(15).and(max)).or(ka.shiftRight(17).and(max))
        );
        return m;
    }
}
