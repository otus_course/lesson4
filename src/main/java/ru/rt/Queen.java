package ru.rt;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Queen extends Chess{

    BigInteger a = new BigInteger("72340172838076673");
    BigInteger h = new BigInteger("9259542123273814144");

    private List<BigInteger> list = new ArrayList<>(){{
        add(new BigInteger("255"));
        add(new BigInteger("65280"));
        add(new BigInteger("16711680"));
        add(new BigInteger("4278190080"));
        add(new BigInteger("1095216660480"));
        add(new BigInteger("280375465082880"));
        add(new BigInteger("71776119061217280"));
        add(new BigInteger("18374686479671623680"));
    }};

    @Override
    BigInteger prepare() {
        BigInteger k = one.shiftLeft(startPos.intValue());
        BigInteger v = new BigInteger("0");
        for (int i = 0; i <= 8; i++) {
            v = v.or(k.shiftLeft(8*i).and(max).or(k.shiftRight(8*i).and(max)));
        }

        BigInteger left = left(v);
        BigInteger right = right(v);

        BigInteger n = new BigInteger("0");
        for (int i = 1; i <= 8; i++) {
            n = n.or(k.shiftLeft(7*i).and(left).and(max).or(k.shiftLeft(9*i).and(right).and(max)))
                    .or(k.shiftRight(7*i).and(right).and(max).or(k.shiftRight(9*i).and(left).and(max)));
        }

        BigInteger noa = new BigInteger("18374403900871474942");
        BigInteger noh = new BigInteger("9187201950435737471");
        BigInteger ka = noa.and(k);
        BigInteger kh = noh.and(k);


        BigInteger h = list.stream().filter(x->!x.xor(k).equals(x.or(k))).findFirst().get();

        BigInteger m = k.not().and(n);
        m = m.or(v.or(h)).or(
                ka.shiftLeft(7).and(max).or(k.shiftLeft(8).and(max)).or(kh.shiftLeft(9).and(max))
                        .or(ka.shiftRight(1).and(max))               .or(kh.shiftLeft(1).and(max))
                        .or(ka.shiftRight(9).and(max)).or(k.shiftRight(8).and(max)).or(kh.shiftRight(7).and(max))).xor(k);
        return m;
    }

    private BigInteger right(BigInteger v) {
        BigInteger rorig = new BigInteger(v.toString());
        BigInteger right = new BigInteger("0");
        while (true) {
            right = right.or(v);
            if (v.equals(h))
                break;
            v = v.shiftLeft(1);
        }
        right = right.xor(rorig);
        return right;
    }

    private BigInteger left(BigInteger v ){
        BigInteger vorig = new BigInteger(v.toString());
        BigInteger left = new BigInteger("0");
        while (true) {
            left = left.or(v);
            if (v.equals(a))
                break;
            v = v.shiftRight(1);
        }
        left = left.xor(vorig);
        return left;
    }
}