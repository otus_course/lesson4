package ru.rt;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Rook extends Chess{

    private List<BigInteger> list = new ArrayList<>(){{
        add(new BigInteger("255"));
        add(new BigInteger("65280"));
        add(new BigInteger("16711680"));
        add(new BigInteger("4278190080"));
        add(new BigInteger("1095216660480"));
        add(new BigInteger("280375465082880"));
        add(new BigInteger("71776119061217280"));
        add(new BigInteger("18374686479671623680"));
    }};

    @Override
    BigInteger prepare() {
        BigInteger k = one.shiftLeft(startPos.intValue());
        BigInteger v = new BigInteger("0");
        for (int i = 1; i <= 8; i++) {
            v = v.or(k.shiftLeft(8*i).and(max).or(k.shiftRight(8*i).and(max)));
        }
        BigInteger h = list.stream().filter(x->!x.xor(k).equals(x.or(k))).findFirst().get();
        BigInteger m = k.not().and(v.or(h));
        return m;
    }
}
